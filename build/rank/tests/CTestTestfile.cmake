# CMake generated Testfile for 
# Source directory: D:/Testir8Sem/rank/tests
# Build directory: D:/Testir8Sem/build/rank/tests
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(itemtest "D:/Testir8Sem/build/bin/itemtest")
set_tests_properties(itemtest PROPERTIES  _BACKTRACE_TRIPLES "D:/Testir8Sem/rank/tests/CMakeLists.txt;7;add_test;D:/Testir8Sem/rank/tests/CMakeLists.txt;0;")
add_test(ranktest "D:/Testir8Sem/build/bin/ranktest")
set_tests_properties(ranktest PROPERTIES  _BACKTRACE_TRIPLES "D:/Testir8Sem/rank/tests/CMakeLists.txt;11;add_test;D:/Testir8Sem/rank/tests/CMakeLists.txt;0;")
add_test(ranktestrev "D:/Testir8Sem/build/bin/ranktestrev")
set_tests_properties(ranktestrev PROPERTIES  _BACKTRACE_TRIPLES "D:/Testir8Sem/rank/tests/CMakeLists.txt;15;add_test;D:/Testir8Sem/rank/tests/CMakeLists.txt;0;")
add_test(sumtest "D:/Testir8Sem/build/bin/sumtest")
set_tests_properties(sumtest PROPERTIES  _BACKTRACE_TRIPLES "D:/Testir8Sem/rank/tests/CMakeLists.txt;19;add_test;D:/Testir8Sem/rank/tests/CMakeLists.txt;0;")
add_test(sumtestrev "D:/Testir8Sem/build/bin/sumtestrev")
set_tests_properties(sumtestrev PROPERTIES  _BACKTRACE_TRIPLES "D:/Testir8Sem/rank/tests/CMakeLists.txt;23;add_test;D:/Testir8Sem/rank/tests/CMakeLists.txt;0;")
